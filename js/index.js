//Script para el MODAL
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal',function (e) {
      console.log('el modal atencion se muestra');

      $('#contactoBtn').removeClass('btn-success');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').prop('disabled',true);

    });
    $('#contacto').on('shown.bs.modal',function (e) {
      console.log('el modal atencion se mostro');
    });
    $('#contacto').on('hide.bs.modal',function (e) {
      console.log('el modal atencion se oculta');
    });
    $('#contacto').on('hidden.bs.modal',function (e) {
      console.log('el modal atencion se oculto');
      $('#contactoBtn').prop('disabled',false);
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-success');


    });

  });